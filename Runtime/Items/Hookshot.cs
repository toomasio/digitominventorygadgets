using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomInventory;

namespace DigitomInventoryGadgets
{
    public class Hookshot : ItemWeaponRanged
    {
        protected override void FireWeapon()
        {
            ammoContainer.FireAmmo(this, muzzle.forward, projectileSpeed);
            EnableFire(false);
        }
    }
}


