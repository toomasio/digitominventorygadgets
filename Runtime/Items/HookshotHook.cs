using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomInventory;
using DigitomPhysics;

namespace DigitomInventoryGadgets
{
    public class HookshotHook : ProjectileStraightYoYo
    {
        [SerializeField] protected bool pullObjectsIn = true;

        protected GameObject grabbedObject;
        protected Vector3 grabHitOffset;

        protected override void Start()
        {
            base.Start();
            stopped = true;
        }

        public override void LaunchProjectile(ItemWeaponRanged owner, float speed, Vector3 direction)
        {
            base.LaunchProjectile(owner, speed, direction);
            stopped = false;
        }

        protected override void DetectCastOnFirst(HitInfo hitInfo)
        {
            if (reversing) return;
            grabbedObject = hitInfo.collider.gameObject;
            grabHitOffset = hitInfo.collider.transform.position - hitInfo.hitPoint;
            base.DetectCastOnFirst(hitInfo);
        }

        protected override void MoveProjectile()
        {
            base.MoveProjectile();

            if (reversing && grabbedObject != null)
            {
                grabbedObject.transform.position = detectPosition.position + grabHitOffset;
            }
        }

        protected override void OnReturn()
        {
            base.OnReturn();
            owner.EnableFire(true);
        }
    }
}


